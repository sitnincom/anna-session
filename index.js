"use strict";

const Promise = require("bluebird");
const AModule = require("anna/AModule");
const session = require('express-session');
const RedisStore = require('connect-redis')(session);

module.exports = class SessionModule extends AModule {
    constructor (core, settings) {
        super(core, settings);

        this.requiredModules = ["anna-express", "anna-redis"];
    }

    get express () {
        return this.modules.get("anna-express").app;
    }

    _initSession () {
        this.express.use(session({
            store: new RedisStore({
                client: this.modules.get("anna-redis").client
            }),
            resave: true,
            saveUninitialized: false,
            secret: this.settings.secret,
            proxy: true,
            cookie: {
                httpOnly: true,
                secure: true,
                maxAge: 86400000 * this.settings.maxAge // 86400000 = 24 * 60 * 60 * 1000
            }
        }));
    }

    init () {
        this.log.debug(`${this.constructor.name}.init()`);
        return new Promise((resolve, reject) => {
            try {
                this._initSession();
            } catch (err) {
                reject(err);
            }
            resolve();
        });
    }
}
